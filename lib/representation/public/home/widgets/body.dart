import 'package:flutter/material.dart';
import 'package:ocr/representation/public/home/widgets/ocr.dart';
import 'package:ocr/representation/public/home/widgets/ocr_test.dart';


class Body extends StatelessWidget {
  const Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Center(
        child: OrcSimple(),
      ),
    );
  }
}
