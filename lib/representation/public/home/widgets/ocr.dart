import 'package:flutter/material.dart';
import 'package:flutter_mobile_vision_2/flutter_mobile_vision_2.dart';
import 'package:flutter_tts/flutter_tts.dart';


class OrcSimple extends StatefulWidget {
  const OrcSimple({Key? key}) : super(key: key);

  @override
  State<OrcSimple> createState() => _OrcSimpleState();
}

class _OrcSimpleState extends State<OrcSimple> {

  bool isInitilized = false;
  List<OcrText> ocrTextList = [];
  FlutterTts flutterTts = FlutterTts();
  // File _image;

  int? _cameraOcr = FlutterMobileVision.CAMERA_BACK;
  bool _autoFocusOcr = true;
  bool _torchOcr = false;
  bool _multipleOcr = false;
  bool _waitTapOcr = false;
  bool _showTextOcr = true;
  Size? _previewOcr;
  List<OcrText> _textsOcr = [];

  @override
  void initState() {
    FlutterMobileVision.start().then((value) {
      isInitilized = true;
    });
    super.initState();
  }


  void speakText(String text) async {
    await flutterTts.setLanguage('fr-FR'); // Définir la langue (facultatif)
    await flutterTts.setPitch(1.0); // Définir le pitch (facultatif)
    await flutterTts.speak(text);
  }


  _startScan() async{
    try{
      ocrTextList = await FlutterMobileVision.read(
        waitTap: true,
        fps: 5,
        flash: false,
      );
      speakTexts(); // Lire le texte après la lecture OCR
      setState(() {}); // Rafraîchir l'affichage avec les nouvelles données
    }catch(e){
      print('Erreur de lecture OCR : $e');
    }
  }

  Future<void> speakTexts() async {
    await flutterTts.setLanguage('fr-FR'); // Définir la langue (facultatif)
    await flutterTts.setPitch(1.0); // Définir le pitch (facultatif)

    for (OcrText text in ocrTextList) {
      await flutterTts.speak(text.value);
    }
  }

  // Future<void> _getImage(ImageSource source) async {
  //   final pickedFile = await ImagePicker().getImage(source: source);
  //   if (pickedFile != null) {
  //     setState(() {
  //       _image = File(pickedFile.path);
  //       _extractTextFromImage();
  //     });
  //   }
  // }


  @override
  void dispose() {
    flutterTts.stop(); // Arrêter la synthèse vocale avant de quitter l'écran
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Column(
      children: [
        SizedBox(height: 60,),
        Container(
          height: 600,
          color: Colors.white,
          child: ListView.builder(
            itemCount: ocrTextList.length,
            itemBuilder: (BuildContext context, int index) {
              return ListTile(
                title: Text(ocrTextList[index].value),
              );
            },
          ),
        ),
        const SizedBox(height: 40,),
        ElevatedButton(onPressed: () => _startScan(), child: Text("Camera")),
        const SizedBox(height: 20,),
      ],
    );
  }
}