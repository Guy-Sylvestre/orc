import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

import '../../representation/public/home/home_screen.dart';

Route<dynamic>? onGenerateRoute (settings){
  switch (settings.name) {
    case "/home":
      return PageTransition(
        child: const HomeScreen(),
        type: PageTransitionType.rightToLeft,
        settings: settings,
        duration: const Duration(milliseconds: 300),
        reverseDuration: const Duration(milliseconds: 300),
      );
    default:
      return null;
  }
}